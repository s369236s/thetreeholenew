﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public int HP;
    public int numOfHearts;

    public Image[] hearts;

    public Sprite heart;
    public Sprite none;


    public GameObject HPContainer;
    public GameObject HPText;
    public Animator animator;

    void Update()
    {
        HP = GameObject.FindGameObjectWithTag("Kent").GetComponent<Combat>().HP;
        if (HP <= 0)
        {
            GameManager.instance.isDialogueOpen = true;
            animator.SetTrigger("Dead");
        }
        if (GameManager.instance.isDialogueOpen)
        {
            HPText.SetActive(false);
            HPContainer.SetActive(false);
        }
        else
        {
            StartCoroutine(Wait());
        }
        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < HP)
            {
                hearts[i].sprite = heart;
            }
            else
            {
                hearts[i].sprite = none;
            }
            if (i < numOfHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.2f);
        if (!GameManager.instance.isDialogueOpen)
        {
            HPContainer.SetActive(true);
            HPText.SetActive(true);
        }
    }
    void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
