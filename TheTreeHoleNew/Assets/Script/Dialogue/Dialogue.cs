﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Dialogue
{
    [Header("對話名字")]
    public string name;


    [Header("對話內容")]
    [TextArea(3, 10)]
    public string[] sentences;
}
