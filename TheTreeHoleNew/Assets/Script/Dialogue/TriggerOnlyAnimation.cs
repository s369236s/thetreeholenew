﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOnlyAnimation : MonoBehaviour
{
    public int dialogueStatus;
    public Animator animator;

    public string animatorTriggerName;
    void Update()
    {
        if (GameManager.instance.dialogueStatus == dialogueStatus)
        {
            animator.SetTrigger(animatorTriggerName);
            this.enabled = false;
        }
    }
}
