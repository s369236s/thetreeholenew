﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerStatus : MonoBehaviour
{
    public int diagloueStatus = 0;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Kent"))
        {
            GameManager.instance.dialogueStatus = diagloueStatus;
            Destroy(gameObject);
        }
    }
}
