﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTriggerWithAnimation : MonoBehaviour
{
    [Header("對話編號")]
    public int dialogueStatus;
    [Header("要觸發的對話")]
    public DialogueTrigger dialogueTrigger;
    [Header("要觸發的動畫")]
    public Animator animator;
    [Header("要觸發的動畫名稱")]
    public string animator_name;
    void Update()
    {
        if (GameManager.instance.dialogueStatus == dialogueStatus)
        {
            dialogueTrigger.TriggerDialogue();
            animator.SetTrigger(animator_name);
            this.enabled = false;
        }
    }
}
