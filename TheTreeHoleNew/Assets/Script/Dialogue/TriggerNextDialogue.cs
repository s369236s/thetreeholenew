﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerNextDialogue : MonoBehaviour
{
    public int dialogueStatus;
    public DialogueTrigger dialogueTrigger;
    void Update()
    {
        if (GameManager.instance.dialogueStatus == dialogueStatus)
        {
            dialogueTrigger.TriggerDialogue();
            this.enabled = false;
        }
    }
}
