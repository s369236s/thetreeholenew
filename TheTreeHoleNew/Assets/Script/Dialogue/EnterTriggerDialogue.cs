﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterTriggerDialogue : MonoBehaviour
{
    public DialogueTrigger dialogueTrigger;

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.tag);
        if (other.CompareTag("Kent"))
        {
            dialogueTrigger.TriggerDialogue();
            Collider2D col2Ds = GetComponent<Collider2D>();
            Destroy(this.gameObject);
        }
    }
}
