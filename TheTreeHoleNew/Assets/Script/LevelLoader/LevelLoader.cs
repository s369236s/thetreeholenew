﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Animator FadeTransition;
    public float FadeTime = 1f;

    public void LoadNextLevel(int levelIdx, int waitSecond)
    {
        StartCoroutine(WaitLoad(levelIdx + 1, waitSecond));
    }

    IEnumerator WaitLoad(int LevelIdx, int waitSecond)
    {
        yield return new WaitForSeconds(waitSecond);
        FadeTransition.SetTrigger("Start");
        yield return new WaitForSeconds(FadeTime);
        SceneManager.LoadScene(LevelIdx);
        GameManager.instance.isDialogueOpen = true;
    }
}
