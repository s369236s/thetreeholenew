﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using System.IO;
public class AudioManager : MonoBehaviour
{
    public AudioMixerGroup MyMixer;
    public string soundName;
    public Sound[] sounds;
    public AudioMixer audioMixer;
    void Awake()
    {

        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.clip = sound.clip;
            sound.source.loop = sound.loop;
            sound.source.outputAudioMixerGroup = MyMixer;
        }
    }


    void Start()
    {

        PlayMusic(soundName);
    }

    // Update is called once per frame
    public void PlayMusic(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }
}
