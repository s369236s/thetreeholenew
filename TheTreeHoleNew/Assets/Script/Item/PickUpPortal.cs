﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpPortal : MonoBehaviour
{
    public bool isHover = false;
    public int levelIndex;
    protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isHover)
        {
            GameObject levelLoader = GameObject.Find("LevelLoader");
            LevelLoader lL = levelLoader.GetComponent<LevelLoader>();
            lL.LoadNextLevel(levelIndex, 0);
            this.enabled = false;
        }
    }
    protected void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.name);
        if (other.CompareTag("Kent"))
        {
            isHover = true;
        }
    }
    protected void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Kent"))
        {
            isHover = false;
        }
    }
}
