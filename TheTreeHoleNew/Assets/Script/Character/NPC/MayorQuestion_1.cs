﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MayorQuestion_1 : MonoBehaviour
{
    public GameObject exclamationMark;
    public DialogueTrigger dialogueTrigger;
    private bool isHover = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isHover)
        {
            dialogueTrigger.TriggerDialogue();
            exclamationMark.SetActive(false);
            this.enabled = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Kent"))
        {
            exclamationMark.GetComponent<Animator>().SetBool("IsHover", true);
            isHover = true;

        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Kent"))
        {
            isHover = false;
            exclamationMark.GetComponent<Animator>().SetBool("IsHover", false);
        }
    }
}
