﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackSmithQuestion_2 : MonoBehaviour
{
    public int dialogueStatus = 0;
    public GameObject exclamationMark;
    public DialogueTrigger dialogueTrigger;
    private bool isHover = false;

    void Update()
    {
        if (GameManager.instance.dialogueStatus == dialogueStatus)
        {
            exclamationMark.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.E) && isHover)
        {
            dialogueTrigger.TriggerDialogue();
            exclamationMark.SetActive(false);
            this.enabled = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Kent"))
        {
            if (exclamationMark.GetComponent<Animator>().isActiveAndEnabled)
                exclamationMark.GetComponent<Animator>().SetBool("IsHover", true);
            isHover = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Kent"))
        {
            isHover = false;
            if (exclamationMark.GetComponent<Animator>().isActiveAndEnabled)
                exclamationMark.GetComponent<Animator>().SetBool("IsHover", false);
        }
    }
}
