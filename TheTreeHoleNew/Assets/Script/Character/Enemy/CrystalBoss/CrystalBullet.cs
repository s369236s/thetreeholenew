﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalBullet : MonoBehaviour
{
    public int shootDamage;
    public float bulletSpeed = 20f;
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = transform.right * bulletSpeed;
        StartCoroutine(DestroySelf());

    }
    IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Kent_Hitbox")
        {
            if (!other.GetComponentInParent<Combat>().isRecovery)
            {
                other.GetComponentInParent<Combat>().TakeDamage(shootDamage);
            }
        }
    }
}
