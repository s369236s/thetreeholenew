﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalSpike : MonoBehaviour
{


    [Header("Spike位置")]
    public Transform spikePositon;
    [Header("上升速度")]
    public float riseSpeed;
    [Header("下降速度")]
    public float fallSpeed;
    [Header("上升點")]
    private Transform spikeRisePositon;
    [Header("警告物件")]
    public GameObject warningObject;
    private bool isRising = false;
    private bool isDroping = false;
    private Vector3 originPosition;


    void Awake()
    {
        spikeRisePositon = GameObject.FindGameObjectWithTag("Crystal_Spike_Rising_Point").transform;
        spikeRisePositon.position = new Vector2(spikePositon.position.x, spikeRisePositon.position.y);
    }
    void Start()
    {
        originPosition = spikePositon.position;
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);
        Destroy(warningObject);
        isRising = true;
    }
    void Update()
    {
        if (isRising)
            spikePositon.position = Vector2.MoveTowards(spikePositon.position, spikeRisePositon.position, riseSpeed * Time.deltaTime);
        else
        {
            spikePositon.position = Vector2.MoveTowards(spikePositon.position, originPosition, fallSpeed * Time.deltaTime);
        }
        if (spikePositon.position == spikeRisePositon.position)
        {
            isDroping = true;
            isRising = false;
        }
        if (isDroping && spikePositon.position == originPosition)
        {
            Destroy(gameObject);
        }
    }

}
