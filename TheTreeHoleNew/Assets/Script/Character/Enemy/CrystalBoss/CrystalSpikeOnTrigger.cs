﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalSpikeOnTrigger : MonoBehaviour
{
    [Header("尖刺傷害")]
    public int spikeDamage;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Kent_Hitbox")
        {
            if (!other.GetComponentInParent<Combat>().isRecovery)
            {
                other.GetComponentInParent<Combat>().TakeDamage(spikeDamage);
            }
        }
    }
}
