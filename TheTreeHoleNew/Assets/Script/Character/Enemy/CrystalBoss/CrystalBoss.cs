using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalBoss : Boss
{
    public enum Status
    {
        SPELLIDLING,
        SHOOTING,
        SPIKING,
        BEAMING,
        MOVING,
        DYING
    }

    [Header("Boss狀態")]
    public Status bossStatus;




    [Header("瞬移速度")]
    public float shootingMoveSpeed = 0;
    [Header("射擊位置")]
    public Transform shootingPosition;
    [Header("施法位置")]
    public Transform spellingPosition;
    [Header("射擊Prefab")]
    public GameObject bulletPrefab;
    [Header("射擊間距")]
    public float shootingTime = 1f;
    [Header("停下射擊次數")]
    public int limitShot = 0;
    [Header("總共射擊次數")]
    public int totalShot = 0;
    [Header("地刺Prefab")]
    public GameObject spikePrefab;
    [Header("尖刺間距")]
    public float spikingTime = 1f;
    [Header("地刺生成範圍1")]
    public float spikeRange_1;
    [Header("地刺生成範圍2")]
    public float spikeRange_2;
    [Header("停下尖刺次數")]
    public int limitSpike = 0;
    [Header("總共尖刺次數")]
    public int totalSpike = 0;

    [Header("施法後停下時間")]
    public float idlingTime = 0;
    [Header("光束物件")]
    public GameObject beamObject;

    [Header("光束動畫")]
    public Animator beamAnimatior;


    [Header("開始光束位置")]
    public Transform beamingPosition;
    [Header("開始結束位置")]
    public Transform endBeamingPosition;

    [Header("死掉位置")]
    public Transform dyingPosition;
    [Header("死掉對話")]
    public DialogueTrigger dyingDialogueTrigger;

    [Header("無敵礦石")]
    public GameObject orbObject;
    private Transform kentTransform;
    private bool isMoving = false;
    private bool isShooting = false;
    private bool isSpiking = false;
    private bool isBeaming = false;
    private bool startBeaming = false;
    private bool isIdling = false;
    private bool kentTransformIsSaved = false;
    private bool isDyingDialogue = false;


    private Vector3 temp;




    // public int scaleSize;

    private bool facingRight = true;
    new void Start()
    {
        base.Start();
        kentTransform = GameObject.FindGameObjectWithTag("Kent").transform;
    }

    // Update is called once per frame
    void Update()
    {
        isBattleBegun = CrystalBossManager.instance.isBattleBegun;
        if (CrystalBossManager.instance.isBattleBegun)
        {
            Battle();
        }
        if ((bossStatus == Status.DYING) && !CrystalBossManager.instance.isBattleBegun)
        {
            DyingAnimation();
        }
    }

    void DyingAnimation()
    {
        transform.position = Vector2.MoveTowards(this.transform.position, dyingPosition.position, shootingMoveSpeed * Time.deltaTime);
        if ((transform.position == dyingPosition.position) && !isDyingDialogue)
        {
            isDyingDialogue = true;
            dyingDialogueTrigger.TriggerDialogue();
        }
    }

    void Battle()
    {
        Turn();
        if (!isMoving)
        {
            if (bossStatus == Status.MOVING)
            {
                if (!kentTransformIsSaved)
                    temp = GameObject.FindGameObjectWithTag("Kent").transform.position;
                kentTransformIsSaved = true;
                SuddenlyMove(Status.SHOOTING);
            }
            if (bossStatus == Status.SHOOTING && !isShooting)
            {
                StartCoroutine(Shoot());
                if (totalShot >= limitShot)
                {
                    bossStatus = Status.SPIKING;
                    totalShot = 0;
                }
            }
            if (bossStatus == Status.SPIKING && !isSpiking)
            {
                transform.position = Vector2.MoveTowards(this.transform.position, spellingPosition.position, shootingMoveSpeed * Time.deltaTime);
                if (spellingPosition.position == transform.position)
                {
                    StartCoroutine(Spike());
                    if (totalSpike >= limitSpike)
                    {
                        totalSpike = 0;
                        bossStatus = Status.SPELLIDLING;
                    }
                }
            }
            if (bossStatus == Status.SPELLIDLING && !isIdling)
            {
                isIdling = true;
                StartCoroutine(Idle());
            }
            if (bossStatus == Status.BEAMING && !isBeaming)
            {
                if (!startBeaming)
                {
                    transform.position = Vector2.MoveTowards(this.transform.position, beamingPosition.position, shootingMoveSpeed * Time.deltaTime);
                    if (beamingPosition.position == transform.position)
                    {
                        beamObject.SetActive(true);
                        beamAnimatior.SetTrigger("Show");
                        startBeaming = true;
                    }
                }
                else
                {
                    transform.position = Vector2.MoveTowards(this.transform.position, endBeamingPosition.position, shootingMoveSpeed * Time.deltaTime);
                    if (transform.position == endBeamingPosition.position)
                    {
                        startBeaming = false;
                        bossStatus = Status.MOVING;
                        beamObject.SetActive(false);
                    }
                }

            }
        }
    }

    void AfterDying()
    {
        orbObject.SetActive(true);
        gameObject.SetActive(false);
    }



    void SuddenlyMove(Status targetStatus)
    {
        transform.position = Vector2.MoveTowards(this.transform.position, temp, shootingMoveSpeed * Time.deltaTime);
        if (transform.position == temp)
        {
            kentTransformIsSaved = false;
            isMoving = false;
            bossStatus = targetStatus;
        }
    }

    override protected void Die()
    {
        bossStatus = Status.DYING;
        CrystalBossManager.instance.isBattleBegun = false;
    }


    IEnumerator Idle()
    {
        yield return new WaitForSecondsRealtime(idlingTime);
        isIdling = false;
        bossStatus = Status.BEAMING;
    }

    IEnumerator Shoot()
    {

        isShooting = true;
        Instantiate(bulletPrefab, shootingPosition.position, shootingPosition.rotation);
        totalShot++;
        yield return new WaitForSeconds(shootingTime);
        isShooting = false;
    }


    IEnumerator Spike()
    {
        isSpiking = true;
        Instantiate(spikePrefab, new Vector3(Random.Range(spikeRange_1, spikeRange_2), -3.33f, -4.359152f), shootingPosition.rotation);
        totalSpike++;
        yield return new WaitForSeconds(spikingTime);
        isSpiking = false;
    }



    void Turn()
    {

        if ((transform.position - kentTransform.position).x > 0 && facingRight)
        {
            Flip();
        }
        else if ((transform.position - kentTransform.position).x < 0 && !facingRight)
        {
            Flip();
        }
    }
    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0, 180f, 0);
    }
}
