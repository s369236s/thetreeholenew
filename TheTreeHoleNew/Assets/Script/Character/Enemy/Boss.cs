﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    [Header("敵人生命")]
    public int HP = 5;
    [Header("敵人傷害")]
    public int enemyDamage = 1;

    [Header("敵人閃爍時間")]
    public float flashTime = 0.5f;
    [Header("敵人無敵")]
    public bool isRecovery = false;
    [Header("是否開始戰鬥")]
    [SerializeField]
    protected bool isBattleBegun = false;
    protected int takeDamageCount = 0;

    private SpriteRenderer spriteRenderer;
    private Color originalColor;

    private Shader shaderGUItext;
    private Shader shaderSpritesDefault;

    protected Animator enemyAnimator;

    public void TakeDamage(int damage)
    {
        if (isBattleBegun)
        {
            HP -= damage;
            takeDamageCount++;
            if (HP > 0)
            {
                FlashColor(flashTime);
            }
            if (HP <= 0)
            {
                FlashColor(flashTime);
                Die();
            }
        }
    }

    protected void Start()
    {
        enemyAnimator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalColor = spriteRenderer.color;
        shaderGUItext = Shader.Find("GUI/Text Shader");
        shaderSpritesDefault = Shader.Find("Sprites/Default");
    }

    void FlashColor(float flashTime)
    {
        isRecovery = true;
        spriteRenderer.material.shader = shaderGUItext;
        spriteRenderer.color = Color.white;
        Invoke("ResetColor", flashTime);
    }
    void ResetColor()
    {
        isRecovery = false;
        spriteRenderer.material.shader = shaderSpritesDefault;
        spriteRenderer.color = originalColor;
    }

    protected virtual void Die()
    {

    }
}
