﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordRatShock : MonoBehaviour
{
    public int shootDamage;
    public float shockSpeed = 20f;
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = transform.right * shockSpeed;
        StartCoroutine(DestroySelf());

    }

    IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
    // Update is called once per frame

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Kent_Hitbox")
        {
            if (!other.GetComponentInParent<Combat>().isRecovery)
            {
                other.GetComponentInParent<Combat>().TakeDamage(shootDamage);
            }
        }
    }
}
