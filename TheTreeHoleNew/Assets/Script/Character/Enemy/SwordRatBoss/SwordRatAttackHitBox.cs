﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordRatAttackHitBox : MonoBehaviour
{
    [Header("揮劍攻擊碰撞體")]
    [SerializeField]
    private PolygonCollider2D[] swingColliders;
    private int currentSwingColliderIndex = 0;

    [Header("衝刺攻擊碰撞體")]
    [SerializeField]
    private PolygonCollider2D[] chargeColliders;
    private int currentChargeColliderIndex = 0;

    public void SetAttackSwingColliderForSprite(int spriteNum)
    {
        swingColliders[currentSwingColliderIndex].enabled = false;
        currentSwingColliderIndex = spriteNum;
        swingColliders[currentSwingColliderIndex].enabled = true;
    }
    public void SetAttackChargeColliderForSprite(int spriteNum)
    {
        chargeColliders[currentChargeColliderIndex].enabled = false;
        currentChargeColliderIndex = spriteNum;
        chargeColliders[currentChargeColliderIndex].enabled = true;
    }
    public void DisableAllAttackHitBox()
    {
        currentSwingColliderIndex = 0;
        currentChargeColliderIndex = 0;
    }
}
