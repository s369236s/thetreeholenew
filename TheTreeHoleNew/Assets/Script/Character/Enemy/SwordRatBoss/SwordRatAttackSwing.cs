﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordRatAttackSwing : MonoBehaviour
{
    public int swingDamage;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Kent_Hitbox")
        {
            if (!other.GetComponentInParent<Combat>().isRecovery)
            {

                other.GetComponentInParent<Combat>().TakeDamage(swingDamage);
            }
        }
    }
}
