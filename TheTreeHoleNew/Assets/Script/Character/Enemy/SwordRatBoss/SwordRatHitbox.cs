﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordRatHitbox : MonoBehaviour
{
    [Header("走路碰撞體")]
    [SerializeField]
    private PolygonCollider2D[] walkColliders;
    private int currentWalkColliderIndex = 0;

    [Header("衝刺碰撞體")]
    [SerializeField]
    private PolygonCollider2D[] chargeColliders;
    private int currentChargeColliderIndex = 0;

    [Header("揮劍碰撞體")]
    [SerializeField]
    private PolygonCollider2D[] swingColliders;
    private int currentSwingColliderIndex = 0;

    [Header("閒置碰撞體")]
    [SerializeField]
    private PolygonCollider2D[] idleColliders;
    private int currentIdleColliderIndex = 0;


    public void SetWalkColliderForSprite(int spriteNum)
    {
        walkColliders[currentWalkColliderIndex].enabled = false;
        currentWalkColliderIndex = spriteNum;
        walkColliders[currentWalkColliderIndex].enabled = true;
    }
    public void SetSwingColliderForSprite(int spriteNum)
    {
        swingColliders[currentSwingColliderIndex].enabled = false;
        currentSwingColliderIndex = spriteNum;
        swingColliders[currentSwingColliderIndex].enabled = true;
    }
    public void SetChargeColliderForSprite(int spriteNum)
    {
        chargeColliders[currentChargeColliderIndex].enabled = false;
        currentChargeColliderIndex = spriteNum;
        chargeColliders[currentChargeColliderIndex].enabled = true;
    }
    public void SetIdleColliderForSprite(int spriteNum)
    {
        idleColliders[currentIdleColliderIndex].enabled = false;
        currentIdleColliderIndex = spriteNum;
        idleColliders[currentIdleColliderIndex].enabled = true;
    }
    public void ResetAllCollider()
    {
        Debug.Log(swingColliders.Length);

    }
}
