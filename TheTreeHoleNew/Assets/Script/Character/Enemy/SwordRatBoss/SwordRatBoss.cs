﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordRatBoss : Boss
{

    [Header("偵錯")]
    public bool debugMode = false;
    enum BossStatus
    {
        MOVING,
        SWING,
        CHARGING,
        SHOCKING,

        DYING,
    }

    [Header("狀態")]
    [SerializeField]
    private BossStatus bossStatus;


    [Header("移動參數")]
    public float moveSpeed = 0f;
    public float chaseRadius = 0f;
    public float stopChaseDistance = 0f;
    public float stuckDistance = 0f;
    public float stuckMoveSpeed = 0f;
    public int getOutStuckDistance = 0;

    [Header("揮劍參數")]
    public int swingTotalTime = 0;
    public int limitSwingTimes = 0;

    [Header("突刺參數")]
    public int chargeSpeed = 0;

    [Header("階段二生命")]
    public int stageTwoHP = 0;
    public int stageTwoMultiplier = 0;

    [Header("衝擊波物件")]
    public GameObject shockPrefab;

    [Header("衝擊波射擊點")]

    public Transform shockPoint;
    [Header("衝擊波參數")]
    public int totalShock = 0;
    public int limitShock = 0;
    public float shockingTime = 0f;

    [Header("死掉對話")]
    public DialogueTrigger dyingDialogueTrigger;

    [Header("魔法土壤")]
    public GameObject fakeSoilObject;
    public GameObject realSoilObject;


    private bool isMoving;

    private bool isSwing;
    private bool isCharging;
    private Transform kentTransform;
    private Vector3 tempKentPosition;
    private bool isKentPositionSaved = false;
    private bool isStuck = false;
    private bool isShocking = false;
    public bool isStageTwo = false;
    private bool isChargeBreak = false;
    private bool facingRight = true;
    private bool isDyingDialogue = false;
    private SpriteRenderer spriteRenderer;
    private Color originalColor;

    private Shader shaderGUItext;
    private Shader shaderSpritesDefault;
    new void Start()
    {
        base.Start();
        kentTransform = GameObject.FindGameObjectWithTag("Kent").GetComponent<Transform>();
        enemyAnimator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalColor = spriteRenderer.color;
        shaderGUItext = Shader.Find("GUI/Text Shader");
        shaderSpritesDefault = Shader.Find("Sprites/Default");
    }


    void Update()
    {
        isBattleBegun = SwordRatBossManager.instance.isBattleBegun;
        if (isBattleBegun)
        {
            fakeSoilObject.SetActive(false);
            Battle();
        }
        if (HP < stageTwoHP && !isStageTwo)
        {
            isStageTwo = true;
            Debug.Log(123);
            moveSpeed += stageTwoMultiplier;
        }
        if ((bossStatus == BossStatus.DYING) && !SwordRatBossManager.instance.isBattleBegun)
        {
            StartCoroutine(DyingAnimation());
        }
    }


    void Battle()
    {
        if (!isSwing && !isCharging)
        {
            Turn();
        }
        if (bossStatus == BossStatus.MOVING && !isMoving)
        {
            Chase();
        }
        if (bossStatus == BossStatus.SWING && !isSwing)
        {
            isSwing = true;
            Swing();
        }
        if (bossStatus == BossStatus.CHARGING)
        {
            if (!isKentPositionSaved)
            {
                tempKentPosition = new Vector3(kentTransform.position.x, transform.position.y);
                isKentPositionSaved = true;
            }
            isCharging = true;
            Charge();
        }
        if (bossStatus == BossStatus.SHOCKING && !isShocking)
        {
            StartCoroutine(Shock());
            if (totalShock >= limitShock)
            {
                Debug.Log("tesdt");
                bossStatus = BossStatus.MOVING;
                totalShock = 0;
            }
        }

    }

    void Chase()
    {
        if (kentTransform != null && HP > 0)
        {
            float distance = (transform.position - kentTransform.position).sqrMagnitude;
            if (distance < chaseRadius && distance > stopChaseDistance)
            {
                enemyAnimator.SetFloat("Speed", 1);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(kentTransform.position.x, transform.position.y), moveSpeed * Time.deltaTime);
                Turn();
            }

            if (distance < stopChaseDistance && distance > stuckDistance)
            {
                Turn();
                enemyAnimator.SetFloat("Speed", 0);
                bossStatus = BossStatus.SWING;
            }
            if (distance < stuckDistance)
            {
                if (transform.position.x < 0 && !isStuck)
                {
                    getOutStuckDistance = Mathf.Abs(getOutStuckDistance);
                }
                if (transform.position.x > 0 && !isStuck)
                {
                    if (getOutStuckDistance > 0 && !isStuck) getOutStuckDistance *= -1;
                }
                isStuck = true;
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(kentTransform.position.x + getOutStuckDistance, transform.position.y), stuckMoveSpeed * Time.deltaTime);
            }
        }
    }

    void Swing()
    {
        isStuck = false;
        enemyAnimator.Play("Swing");
        swingTotalTime++;
    }

    IEnumerator Shock()
    {
        isShocking = true;
        enemyAnimator.Play("Swing");
        Instantiate(shockPrefab, shockPoint.position, shockPoint.rotation);
        yield return new WaitForSeconds(shockingTime);
        totalShock++;
        isShocking = false;
    }


    void EndSwing()
    {
        if (bossStatus == BossStatus.SWING)
        {
            if (swingTotalTime < limitSwingTimes)
            {
                bossStatus = BossStatus.MOVING;
            }
            else
            {
                bossStatus = BossStatus.CHARGING;
                swingTotalTime = 0;
            }
        }
        isSwing = false;
    }

    void Charge()
    {

        if (transform.position == tempKentPosition)
        {
            enemyAnimator.SetTrigger("ChargeToIdle");
            if (!isChargeBreak)
                StartCoroutine(ChargeBreak());
        }
        else
        {
            enemyAnimator.Play("Charge");
            transform.position = Vector2.MoveTowards(transform.position, tempKentPosition, chargeSpeed * Time.deltaTime);
        }
    }
    IEnumerator ChargeBreak()
    {
        isChargeBreak = true;
        Debug.Log(isStageTwo);
        yield return new WaitForSeconds(2);
        if (isStageTwo)
        {
            bossStatus = BossStatus.SHOCKING;
        }
        else
        {
            bossStatus = BossStatus.MOVING;
        }
        isKentPositionSaved = false;
        isCharging = false;
        isChargeBreak = false;
    }

    void Turn()
    {

        if ((transform.position - kentTransform.position).x > 0 && facingRight)
        {
            Flip();
        }
        else if ((transform.position - kentTransform.position).x < 0 && !facingRight)
        {
            Flip();
        }
    }
    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0, 180f, 0);
    }

    override protected void Die()
    {
        bossStatus = BossStatus.DYING;
        SwordRatBossManager.instance.isBattleBegun = false;
    }
    IEnumerator DyingAnimation()
    {
        if (!isDyingDialogue)
        {
            ResetColor();
            for (int i = 0; i < 3; i++)
            {
                spriteRenderer.color = new Color(1f, 1f, 1f, 0f);
                yield return new WaitForSeconds(0.15f);
                spriteRenderer.color = originalColor;
                yield return new WaitForSeconds(0.15f);
            }
            Destroy(this.gameObject);
            isDyingDialogue = true;
            dyingDialogueTrigger.TriggerDialogue();
            realSoilObject.SetActive(true);
        }
    }
    void ResetColor()
    {
        isRecovery = false;
        spriteRenderer.material.shader = shaderSpritesDefault;
        spriteRenderer.color = originalColor;
    }
    void OnDrawGizmosSelected()
    {
        if (debugMode)
        {
            // Gizmos.DrawWireSphere(transform.position, chaseRadius / 2);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, stuckDistance / 2);
        }
    }
}
