﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : Enemy
{
    [Header("敵人速度")]
    public float speed;
    [Header("敵人追擊範圍")]
    public float radius;

    [Header("敵人大小")]
    public int scaleSize = 5;

    [Header("敵人攻擊距離")]
    public float stopChaseDistance = 1.5f;

    [Header("敵人攻擊間距")]
    public float AttackTime = 1.5f;
    [Header("敵人是否攻擊過")]
    public bool isAttacking = false;

    [Header("敵人攻擊Hitbox")]
    [SerializeField]
    private PolygonCollider2D[] colliders;
    private int currentColliderIndex = 0;

    [Header("偵錯模式")]
    public bool debugMode = false;

    private Transform playerTransform;
    protected new void Start()
    {
        base.Start();
        playerTransform = GameObject.FindGameObjectWithTag("Kent").GetComponent<Transform>();
    }

    // Update is called once per frame
    protected void Update()
    {
        if (playerTransform != null && HP > 0)
        {
            float distance = (transform.position - playerTransform.position).sqrMagnitude;
            if (distance < radius * 3.14 && distance > stopChaseDistance)
            {
                enemyAnimator.SetFloat("Speed", 1);
                enemyAnimator.SetTrigger("Walk");
                transform.position = Vector2.MoveTowards(transform.position, playerTransform.position, speed * Time.deltaTime);
                Turn();
            }
            else
            {
                enemyAnimator.SetFloat("Speed", 0);
            }
            if (distance < stopChaseDistance && !isAttacking)
            {
                enemyAnimator.SetFloat("Speed", 0);
                Attack();
            }
        }
    }
    void Turn()
    {
        Vector3 localScale = transform.localScale;
        if ((transform.position - playerTransform.position).x > 0)
        {
            localScale.x = -scaleSize;
        }
        if ((transform.position - playerTransform.position).x < 0)
        {
            localScale.x = scaleSize;
        }
        transform.localScale = localScale;
    }
    void Attack()
    {
        enemyAnimator.SetTrigger("Attack");
        isAttacking = true;
        StartCoroutine(ResetAttack());
    }
    IEnumerator ResetAttack()
    {
        yield return new WaitForSeconds(AttackTime);
        isAttacking = false;
    }

    public void DisableHitBox()
    {
        colliders[currentColliderIndex].enabled = false;
    }
    public void DisableAllHitBox()
    {
        foreach (Collider2D hitCollider in colliders)
        {
            hitCollider.enabled = false;
        }
    }
    public void SetColliderForSprite(int spriteNum)
    {
        colliders[currentColliderIndex].enabled = false;
        currentColliderIndex = spriteNum;
        colliders[currentColliderIndex].enabled = true;
    }
    void OnDrawGizmosSelected()
    {
        if (debugMode)
        {
            Gizmos.DrawWireSphere(transform.position, radius);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, stopChaseDistance);
        }
    }

}
