﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunToPoint : MonoBehaviour
{
    public int diagloueStatus = 0;
    public Transform point;
    public Animator animator;
    public float speed;

    private bool isTrigger = false;
    void Update()
    {
        if ((GameManager.instance.dialogueStatus == diagloueStatus) || isTrigger)
        {
            isTrigger = true;
            if ((transform.position - point.position).sqrMagnitude > 0)
            {
                animator.SetTrigger("Walk");
                transform.position = Vector2.MoveTowards(transform.position, point.position, speed * Time.deltaTime);
            }
            else
            {
                animator.SetTrigger("Idle");
                isTrigger = false;
                this.enabled = false;
            }
        }
    }
}
