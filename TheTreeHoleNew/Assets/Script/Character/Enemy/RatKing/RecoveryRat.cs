﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecoveryRat : Enemy
{
    public float moveSpeed = 0f;
    private Transform ratKingPosition;
    private bool facingRight = false;
    new void Start()
    {
        base.Start();
        ratKingPosition = GameObject.FindGameObjectWithTag("RatKing").transform;

    }

    // Update is called once per frame
    void Update()
    {
        if (ratKingPosition != null && HP > 0)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(ratKingPosition.position.x, transform.position.y), moveSpeed * Time.deltaTime);
            Turn();
        }
        if (HP <= 0)
        {
            Die();
        }

        void Turn()
        {

            if ((transform.position - ratKingPosition.position).x > 0 && facingRight)
            {
                Flip();
            }
            else if ((transform.position - ratKingPosition.position).x < 0 && !facingRight)
            {
                Flip();
            }
        }
        void Flip()
        {
            facingRight = !facingRight;
            transform.Rotate(0, 180f, 0);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.tag);
        if (other.CompareTag("RatKing"))
        {
            other.GetComponentInParent<RatKing>().Heal(1);
            Die();
        }
    }
    new void Die()
    {
        Destroy(gameObject);
    }
}
