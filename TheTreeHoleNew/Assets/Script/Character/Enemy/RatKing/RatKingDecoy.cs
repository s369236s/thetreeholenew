﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatKingDecoy : Boss
{
    [Header("偵錯")]
    public bool debugMode = false;
    enum BossStatus
    {
        MOVING,
        ATTACK,
        CLONE,
        RECORVERYING,
    }
    [Header("狀態")]
    [SerializeField]
    private BossStatus bossStatus;


    [Header("移動參數")]
    public float moveSpeed = 0f;
    public float chaseRadius = 0f;
    public float stopChaseDistance = 0f;

    [Header("攻擊參數")]

    public int attackTotalTime = 0;
    public int limitAttackTimes = 0;


    private bool isMoving = false;
    private bool isAttacking = false;
    private bool isRecoverySelf = false;
    private bool isCloning = false;
    private Transform kentTransform;
    private Vector3 tempKentPosition;
    private bool facingRight = true;

    new void Start()
    {
        base.Start();
        kentTransform = GameObject.FindGameObjectWithTag("Kent").GetComponent<Transform>();
        enemyAnimator = GetComponent<Animator>();

    }
    void Update()
    {
        if (HP <= 0) Die();
        Battle();
    }

    void Battle()
    {
        if (!isAttacking && !isRecoverySelf && !isCloning)
        {
            Turn();
        }
        if (bossStatus == BossStatus.MOVING && !isMoving)
        {
            Chase();
        }
        if (bossStatus == BossStatus.ATTACK && !isAttacking)
        {
            isAttacking = true;
            Attack();
        }

    }


    void Chase()
    {
        if (kentTransform != null && HP > 0)
        {
            float distance = (transform.position - kentTransform.position).sqrMagnitude;
            if (distance < chaseRadius && distance > stopChaseDistance)
            {
                enemyAnimator.SetFloat("Speed", 1);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(kentTransform.position.x, transform.position.y), moveSpeed * Time.deltaTime);
                Turn();
            }

            if (distance < stopChaseDistance)
            {
                Turn();
                enemyAnimator.SetFloat("Speed", 0);
                bossStatus = BossStatus.ATTACK;
            }

        }
    }

    void Attack()
    {
        enemyAnimator.Play("Attack");
        attackTotalTime++;
    }

    void EndAttack()
    {
        if (bossStatus == BossStatus.ATTACK)
        {
            if (attackTotalTime < limitAttackTimes)
            {
                bossStatus = BossStatus.MOVING;
            }
        }
        isAttacking = false;
    }



    void Turn()
    {

        if ((transform.position - kentTransform.position).x > 0 && facingRight)
        {
            Flip();
        }
        else if ((transform.position - kentTransform.position).x < 0 && !facingRight)
        {
            Flip();
        }
    }
    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0, 180f, 0);
    }

    protected override void Die()
    {
        Destroy(gameObject);
    }
}
