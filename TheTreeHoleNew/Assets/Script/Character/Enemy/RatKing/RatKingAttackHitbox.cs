﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatKingAttackHitbox : MonoBehaviour
{
    [Header("攻擊碰撞體")]
    [SerializeField]
    private PolygonCollider2D[] attackColliders;
    private int currentAttackColliderIndex = 0;

    [Header("尾巴碰撞體")]
    [SerializeField]
    private PolygonCollider2D[] tailColliders;
    private int currentTailColliderIndex = 0;

    public void SetAttackColliderForSprite(int spriteNum)
    {
        attackColliders[currentAttackColliderIndex].enabled = false;
        currentAttackColliderIndex = spriteNum;
        attackColliders[currentAttackColliderIndex].enabled = true;
    }
    public void SetTailColliderForSprite(int spriteNum)
    {
        tailColliders[currentTailColliderIndex].enabled = false;
        currentTailColliderIndex = spriteNum;
        tailColliders[currentTailColliderIndex].enabled = true;
    }
    public void DisableAllAttackHitBox()
    {
        foreach (Collider2D collider2D in attackColliders)
        {
            collider2D.enabled = false;
        }
        foreach (Collider2D collider2D in tailColliders)
        {
            collider2D.enabled = false;
        }
        currentAttackColliderIndex = 0;
        currentTailColliderIndex = 0;
    }
}
