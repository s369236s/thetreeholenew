﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.tag);
        if (other.CompareTag("RatKing"))
        {
            other.GetComponentInParent<RatKing>().Heal(1);
            Destroy(GetComponentInParent<GameObject>());
        }
    }
}
