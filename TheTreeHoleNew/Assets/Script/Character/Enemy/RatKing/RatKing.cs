﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatKing : Boss
{

    [Header("偵錯")]
    public bool debugMode = false;
    enum BossStatus
    {
        MOVING,
        ATTACK,
        TAILING,
        CLONE,
        RECORVERYING,
        DYING,
    }
    [Header("狀態")]
    [SerializeField]
    private BossStatus bossStatus;


    [Header("移動參數")]
    public float moveSpeed = 0f;
    public float chaseRadius = 0f;
    public float stopChaseDistance = 0f;

    [Header("攻擊參數")]

    public int attackTotalTime = 0;
    public int limitAttackTimes = 0;

    [Header("分身參數")]

    public GameObject clonePrefab;


    [Header("回復參數")]
    public int debugCount = 0;
    public int limitDamageCount = 0;
    public Transform summonPositionRight;
    public Transform summonPositionLeft;
    public GameObject ratPrefab;
    public int limitRat = 0;
    public int totalRat = 0;



    [Header("死掉對話")]
    public DialogueTrigger dyingDialogueTrigger;


    private bool isMoving = false;
    private bool isAttacking = false;
    private bool isRecoverySelf = false;
    private bool isCloning = false;
    private bool isSummoning = false;
    private bool isTailing = false;
    private bool isTailed = false;

    private Transform kentTransform;
    private Vector3 tempKentPosition;
    private bool isSummonRight = false;

    private bool isDyingDialogue = false;
    private bool facingRight = true;
    public Collider2D hitBox;
    // private bool isDyingDialogue = false;
    private SpriteRenderer ss;
    private Color oC;



    new void Start()
    {
        base.Start();
        kentTransform = GameObject.FindGameObjectWithTag("Kent").GetComponent<Transform>();
        Turn();
        enemyAnimator = GetComponent<Animator>();
        ss = GetComponent<SpriteRenderer>();
        oC = ss.color;
    }
    void Update()
    {
        isBattleBegun = RatKingBattleManager.instance.isBattleBegun;
        if (isBattleBegun)
        {
            debugCount = takeDamageCount;
            Battle();
            if (GameObject.FindGameObjectsWithTag("RatKingClone").Length > 0 && bossStatus != BossStatus.RECORVERYING)
            {
                hitBox.enabled = false;
            }
            if (GameObject.FindGameObjectsWithTag("RatKingClone").Length <= 0 && bossStatus != BossStatus.RECORVERYING)
            {
                hitBox.enabled = true;
            }
        }
        if ((bossStatus == BossStatus.DYING) && !RatKingBattleManager.instance.isBattleBegun)
        {
            StartCoroutine(DyingAnimation());
        }
    }

    void Battle()
    {
        if (!isAttacking && !isRecoverySelf && !isCloning)
        {
            Turn();
        }
        if (bossStatus == BossStatus.MOVING && !isMoving)
        {
            Chase();
        }
        if (bossStatus == BossStatus.ATTACK && !isAttacking)
        {
            isAttacking = true;
            Attack();
        }
        if (bossStatus == BossStatus.TAILING && !isTailing)
        {
            isTailing = true;
            Tail();
        }

        if (bossStatus == BossStatus.CLONE && !isCloning)
        {
            if (GameObject.FindGameObjectsWithTag("RatKingClone").Length <= 0)
            {
                isCloning = true;
                Clone();
            }
            else
            {
                bossStatus = BossStatus.MOVING;
            }
        }


        if (bossStatus == BossStatus.RECORVERYING)
        {
            if (!isSummoning)
            {
                StartCoroutine(Recovery());
                hitBox.enabled = false;
            }
            if (!isRecoverySelf)
            {
                isRecoverySelf = true;
                enemyAnimator.Play("Recover");
            }
        }
    }



    void Chase()
    {
        if (kentTransform != null && HP > 0)
        {
            float distance = (transform.position - kentTransform.position).sqrMagnitude;
            if (distance < chaseRadius && distance > stopChaseDistance)
            {
                enemyAnimator.SetFloat("Speed", 1);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(kentTransform.position.x, transform.position.y), moveSpeed * Time.deltaTime);
                Turn();
            }

            if (distance < stopChaseDistance)
            {
                Turn();
                enemyAnimator.SetFloat("Speed", 0);
                if (takeDamageCount >= limitDamageCount)
                {
                    bossStatus = BossStatus.RECORVERYING;
                    takeDamageCount = 0;
                }
                else
                {
                    if (attackTotalTime == 2 && !isTailed)
                    {
                        isTailed = true;
                        bossStatus = BossStatus.TAILING;
                    }
                    else
                    {

                        bossStatus = BossStatus.ATTACK;
                    }
                }
            }

        }
    }


    void Attack()
    {
        enemyAnimator.Play("Attack");
        attackTotalTime++;

    }

    void EndAttack()
    {
        if (bossStatus == BossStatus.ATTACK)
        {
            if (attackTotalTime < limitAttackTimes)
            {
                bossStatus = BossStatus.MOVING;
            }
            if (attackTotalTime >= limitAttackTimes)
            {
                if (GameObject.FindGameObjectsWithTag("RatKingClone").Length <= 0)
                    bossStatus = BossStatus.CLONE;
                if (attackTotalTime == 3)
                {
                    isTailed = false;
                }
                attackTotalTime = 0;
            }
        }
        isAttacking = false;
    }

    void Tail()
    {
        enemyAnimator.Play("Tail");
    }
    void EndTail()
    {
        bossStatus = BossStatus.MOVING;
        isTailing = false;
    }
    void Clone()
    {
        if (GameObject.FindGameObjectsWithTag("RatKingClone").Length <= 0)
            enemyAnimator.Play("Clone");
    }//2.215
    void EndClone()
    {
        Instantiate(clonePrefab, new Vector2(transform.position.x + 2.785f, transform.position.y), transform.rotation);
        Instantiate(clonePrefab, new Vector2(transform.position.x - 2.785f, transform.position.y), transform.rotation);
        bossStatus = BossStatus.MOVING;
        isCloning = false;
    }
    IEnumerator Recovery()
    {
        isSummoning = true;
        yield return new WaitForSeconds(3);
        if (isSummonRight && totalRat < limitRat)
        {
            isSummonRight = false;
            Instantiate(ratPrefab, summonPositionRight.position, summonPositionRight.rotation);
        }
        if (!isSummonRight && totalRat < limitRat)
        {
            isSummonRight = true;
            Instantiate(ratPrefab, summonPositionLeft.position, summonPositionRight.rotation);
        }
        totalRat++;
        isSummoning = false;
        if (totalRat >= limitRat && GameObject.FindGameObjectsWithTag("RecoveryRat").Length <= 0)
        {
            totalRat = 0;
            bossStatus = BossStatus.MOVING;
            hitBox.enabled = true;
            enemyAnimator.SetTrigger("EndRecover");
        }
    }


    void Turn()
    {

        if ((transform.position - kentTransform.position).x > 0 && facingRight)
        {
            Flip();
        }
        else if ((transform.position - kentTransform.position).x < 0 && !facingRight)
        {
            Flip();
        }
    }
    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0, 180f, 0);
    }
    public void Heal(int healAmount)
    {
        HP += healAmount;
        StartCoroutine(HealAnimation());
    }

    IEnumerator HealAnimation()
    {
        ss.color = new Color(0.7971f, 1f, 0.8065f, 1f);
        yield return new WaitForSeconds(1);
        ss.color = oC;
    }

    override protected void Die()
    {
        bossStatus = BossStatus.DYING;
        RatKingBattleManager.instance.isBattleBegun = false;
        GameManager.instance.isDialogueOpen = true;
    }
    IEnumerator DyingAnimation()
    {
        if (!isDyingDialogue)
        {
            yield return new WaitForSeconds(1);
            isDyingDialogue = true;
            enemyAnimator.Play("Die");
            yield return new WaitForSeconds(1);
            dyingDialogueTrigger.TriggerDialogue();
        }
    }
}
