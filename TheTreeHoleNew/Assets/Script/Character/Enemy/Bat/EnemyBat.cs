﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBat : Enemy
{
    public float idleSpeed;
    public float speed;
    public float radius;

    Vector3 newPosition;

    private Transform kentTransform;
    private bool facingRight = true;
    new void Start()
    {
        base.Start();
        PositionChange();
        kentTransform = GameObject.FindGameObjectWithTag("Kent").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (kentTransform != null)
        {
            float distance = (transform.position - kentTransform.position).sqrMagnitude;
            if (distance < radius)
            {
                Turn();
                transform.position = Vector2.MoveTowards(transform.position, kentTransform.position, speed * Time.deltaTime);
            }
            else
            {
                if ((transform.position - newPosition).x > 0 && facingRight)
                {
                    Flip();
                }
                else if ((transform.position - newPosition).x < 0 && !facingRight)
                {
                    Flip();
                }
                if (Vector2.Distance(transform.position, newPosition) < 1 && distance > radius)
                    PositionChange();
                transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * idleSpeed);
            }

        }

    }


    void PositionChange()
    {
        newPosition = new Vector2(transform.position.x + Random.Range(-2.0f, 2.0f), transform.position.y + Random.Range(-2.0f, 2.0f));
    }
    void Turn()
    {

        if ((transform.position - kentTransform.position).x > 0 && facingRight)
        {
            Flip();
        }
        else if ((transform.position - kentTransform.position).x < 0 && !facingRight)
        {
            Flip();
        }
    }
    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0, 180f, 0);
    }
}
