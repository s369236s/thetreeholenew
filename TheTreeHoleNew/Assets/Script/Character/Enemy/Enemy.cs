﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("敵人生命")]
    public int HP = 5;
    [Header("敵人傷害")]
    public int enemyDamage = 1;

    [Header("敵人閃爍時間")]
    public float flashTime = 0.5f;
    [Header("敵人無敵")]
    public bool isRecovery = false;

    private SpriteRenderer spriteRenderer;
    private Color originalColor;

    private Shader shaderGUItext;
    private Shader shaderSpritesDefault;

    protected Animator enemyAnimator;

    public void TakeDamage(int damage)
    {
        HP -= damage;
        if (HP > 0)
        {
            FlashColor(flashTime);
        }
        if (HP <= 0)
        {
            FlashColor(flashTime);
            Die();
        }
    }
    protected void Start()
    {
        enemyAnimator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalColor = spriteRenderer.color;
        shaderGUItext = Shader.Find("GUI/Text Shader");
        shaderSpritesDefault = Shader.Find("Sprites/Default");
    }
    void FlashColor(float flashTime)
    {
        isRecovery = true;
        spriteRenderer.material.shader = shaderGUItext;
        spriteRenderer.color = Color.white;
        Invoke("ResetColor", flashTime);
    }
    void ResetColor()
    {
        isRecovery = false;
        spriteRenderer.material.shader = shaderSpritesDefault;
        spriteRenderer.color = originalColor;
    }


    public void Die()
    {
        spriteRenderer.color = originalColor;
        enemyAnimator.SetTrigger("Die");
    }

    protected void DestroyObject()
    {
        StartCoroutine(PlayDeadAnimation());

    }
    IEnumerator PlayDeadAnimation()
    {
        ResetColor();
        for (int i = 0; i < 3; i++)
        {
            spriteRenderer.color = new Color(1f, 1f, 1f, 0f);
            yield return new WaitForSeconds(0.15f);
            spriteRenderer.color = originalColor;
            yield return new WaitForSeconds(0.15f);
        }
        Destroy(this.gameObject);
    }

}
