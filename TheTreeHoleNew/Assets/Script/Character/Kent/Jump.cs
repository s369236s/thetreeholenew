﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public BoxCollider2D boxCol2D;
    [Header("人物動畫")]
    public Animator animator;

    [Header("跳躍參數")]
    public float fallMultiplier = 2.5f;
    public float lowMultiplier = 2f;
    public float jumpVelocity = 6f;
    [Header("偵測地面碰撞")]

    [Header("偵測Layer")]
    public LayerMask Groundlayer;
    public LayerMask Walllayer;

    private Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {

        if (!GameManager.instance.isDialogueOpen)
        {
            Jump_();
        }
    }
    void Jump_()
    {
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb.velocity = Vector2.up * jumpVelocity;
        }
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowMultiplier - 1) * Time.deltaTime;
        }
        if (!IsGrounded())
        {
            animator.SetBool("IsJump", true);
        }
        else
        {
            animator.SetBool("IsJump", false);
        }
    }
    private bool IsGrounded()
    {
        float eHT = 0.5f;
        RaycastHit2D raycastHit = Physics2D.Raycast(boxCol2D.bounds.center, Vector2.down, boxCol2D.bounds.extents.y + eHT, Groundlayer);
        return raycastHit.collider != null;
    }
}
