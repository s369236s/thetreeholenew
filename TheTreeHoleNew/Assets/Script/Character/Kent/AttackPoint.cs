﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPoint : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (!other.GetComponentInParent<Enemy>().isRecovery)
            {
                Debug.Log(this.gameObject.name + " hit " + other.name);
                other.GetComponentInParent<Enemy>().TakeDamage(1);
            }
        }
        if (other.gameObject.CompareTag("Boss"))
        {
            if (!other.GetComponentInParent<Boss>().isRecovery)
            {
                Debug.Log(this.gameObject.name + " hit " + other.name);
                other.GetComponentInParent<Boss>().TakeDamage(1);
            }
        }
    }
}
