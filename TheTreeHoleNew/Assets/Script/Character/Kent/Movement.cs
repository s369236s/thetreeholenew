﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    [Header("人物動畫")]
    public Animator animator;

    [Header("人物速度")]
    public float moveSpeed = 1f;
    [Header("人物大小")]
    public int scaleSize = 5;



    private bool facingRight = true;
    void Update()
    {
        if (!GameManager.instance.isDialogueOpen)
        {
            Move();
        }
        else
        {
            animator.SetFloat("Speed", 0);

        }
    }

    void Move()
    {

        animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));
        transform.position += new Vector3(Input.GetAxis("Horizontal"), 0, 0) * Time.deltaTime * moveSpeed;
        if (!Combat.instance.isAttackingAnimation)
            Turn();
    }

    void Turn()
    {
        Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;

        Vector3 localScale = transform.localScale;
        if ((Input.GetAxis("Horizontal") < 0 || diff.x <= 0) && facingRight)
        {
            if (Input.GetAxis("Horizontal") < 0 && diff.x >= 0)
            {
            }
            else
                Flip();
        }
        else if ((Input.GetAxis("Horizontal") > 0 || diff.x >= 0) && !facingRight)
        {
            if (Input.GetAxis("Horizontal") > 0 && diff.x <= 0)
            {

            }
            else
                Flip();
        }
        transform.localScale = localScale;
    }

    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0, 180f, 0);
    }
}
