﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Combat : MonoBehaviour
{
    public static Combat instance;

    public int HP = 10;
    public bool isAttacking = false;
    public bool isAttackingAnimation = false;

    public int kentDamage = 1;

    public float flashTime = 0.5f;

    public bool isRecovery = false;


    [SerializeField]
    private PolygonCollider2D[] colliders;
    private int currentColliderIndex = 0;

    [HideInInspector]
    public Animator attackAnimator;

    public float disableHitBoxTime = 0.5f;

    private SpriteRenderer spriteRenderer;
    private Color originalColor;
    private int countTime = 0;

    private bool isHealing = false;

    void Awake()
    {
        HP = 10;
        instance = this;
    }

    void Start()
    {

        InvokeRepeating("timer", 1, 1);
        attackAnimator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalColor = spriteRenderer.color;
    }

    void Update()
    {
        Attack();
        Cheat();
    }

    void Cheat()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            Debug.Log(diff);
        }
        if (Input.GetKeyDown(KeyCode.F11))
        {
            HP += 100;
        }

    }
    void timer()
    {
        countTime++;
        if (countTime == 3)
        {
            isHealing = true;
        }
        if (isHealing && HP < 10)
        {
            HP++;
            countTime = 0;
        }
    }

    void Attack()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && !GameManager.instance.isDialogueOpen && !isAttacking)
        {
            isAttacking = true;
            isAttackingAnimation = true;
        }
    }

    public void DisableHitBox()
    {
        colliders[currentColliderIndex].enabled = false;
    }
    public void DisableAllHitBox()
    {
        foreach (Collider2D hitCollider in colliders)
        {
            hitCollider.enabled = false;
        }
    }

    public void SetColliderForSprite(int spriteNum)
    {
        colliders[currentColliderIndex].enabled = false;
        currentColliderIndex = spriteNum;
        colliders[currentColliderIndex].enabled = true;
    }
    void FlashColor(float flashTime)
    {
        isRecovery = true;
        StartCoroutine(PlayHurtAnimation());
    }

    IEnumerator PlayHurtAnimation()
    {
        for (int i = 0; i < 3; i++)
        {
            spriteRenderer.color = new Color(1f, 1f, 1f, 0f);
            yield return new WaitForSeconds(0.15f);
            spriteRenderer.color = originalColor;
            yield return new WaitForSeconds(0.15f);
        }
        spriteRenderer.color = originalColor;
        yield return new WaitForSeconds(0.2f);
        isRecovery = false;
    }
    public void TakeDamage(int damage)
    {
        if (!GameManager.instance.isDialogueOpen)
        {
            HP -= damage;
            countTime = 0;
            isHealing = false;
        }
        if (HP > 0)
        {
            FlashColor(flashTime);
        }
        if (HP <= 0)
        {
            FlashColor(flashTime);
            Die();
        }

    }
    public void Die()
    {
        Debug.Log("Kent Died");
    }
}
