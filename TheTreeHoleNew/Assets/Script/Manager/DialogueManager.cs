﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{

    public Text nameText;
    public Text dialogueText;

    public Animator animator;
    private Queue<string> sentences;

    private bool isSentenceOver = true;
    private bool debugMode = false;
    public int dialogueStatus = 0;
    void Start()
    {
        sentences = new Queue<string>();
    }


    public void DisplayNextSentence()
    {
        if (isSentenceOver || debugMode)
        {
            if (sentences.Count == 0)
            {
                EndDialogue();
                return;
            };
            isSentenceOver = false;
            string sentence = sentences.Dequeue();
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence));
        }

    }

    public void StartDialogue(Dialogue dialogue, int dialogueStatus)
    {

        this.dialogueStatus = dialogueStatus;
        animator.SetBool("IsOpen", true);
        nameText.text = dialogue.name;
        sentences.Clear();
        GameManager.instance.isDialogueOpen = true;
        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }
    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return new WaitForSeconds(0.1f);
        }
        isSentenceOver = true;
    }
    public void EndDialogue()
    {
        animator.SetBool("IsOpen", false);
        GameManager.instance.isDialogueOpen = false;
        GameManager.instance.dialogueStatus = dialogueStatus;

    }
}
