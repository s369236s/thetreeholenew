﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public enum MissonStatus
    {
        ORB,
        SOIL,
        FLAME
    }



    [Header("任務狀態")]
    public MissonStatus currentMisson;

    [Header("對話是否打開")]
    public bool isDialogueOpen = false;

    [Header("偵錯模式")]
    public bool debugMode = false;

    [Header("對話狀態")]
    public int dialogueStatus = 0;

    void Awake()
    {
        instance = this;
    }
    void Update()
    {
        Cheat();
    }
    void Cheat()
    {

        if (Input.GetKeyDown(KeyCode.F12))
        {
            Scene scene = SceneManager.GetActiveScene();
            int t = scene.buildIndex;
            SceneManager.LoadScene(t += 1);
        }

    }
}
