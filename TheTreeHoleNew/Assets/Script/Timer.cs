﻿using System.Collections;

using System.Collections.Generic;

using UnityEngine;

using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    int countTime = 0;

    public Text time_UI;

    void Awake()
    {

        InvokeRepeating("timer", 1, 1);

    }

    void timer()
    {
        countTime++;
        time_UI.text = countTime.ToString();

    }

}