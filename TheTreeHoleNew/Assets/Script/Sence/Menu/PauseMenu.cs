﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public Slider slider;
    public GameObject pauseMenuUI;
    public GameObject OptionObject;
    public GameObject ControllerObject;

    public GameObject menuBackground;
    public GameObject logo;
    public AudioMixer audioMixer;
    public Text resText;
    private float tempVolume;
    private bool isFullscreen = true;
    private bool isOption = false;

    public bool isContoller = false;
    string fileName = "setting.json";
    private bool isGameEnd = false;
    public GameObject emptyButton;
    public GameObject fullButton;
    private string[] res = {
        "1920 x 1080",
        "1600 x 900",
        "1440 x 900"
    };

    class Reslo
    {
        public int width;
        public int height;
    }

    private Reslo[] resolutions ={
        new Reslo{ width =1920,height=1080},
        new Reslo{ width =1600,height=900},
        new Reslo{ width =1440,height=900}
    };
    private int resIdx = 0;
    void Start()
    {
        GameData newData = new GameData
        {
            isFullscreen = this.isFullscreen,
            volumeData = tempVolume,
            isGameEnd = this.isGameEnd,
            resIdx = this.resIdx
        };
        if (!Directory.Exists(System.IO.Path.Combine(Application.dataPath, "Setting")))
        {
            Directory.CreateDirectory(System.IO.Path.Combine(Application.dataPath, "Setting"));
        }
        fileName = Path.Combine(System.IO.Path.Combine(Application.dataPath, "Setting"), fileName);
        if (!File.Exists(fileName))
        {
            newData.volumeData = 1.0f;
            FileStream fs = File.Create(fileName);
            fs.Close();
            string saveString = JsonUtility.ToJson(newData);
            File.WriteAllText(fileName, saveString);
            setVolume(newData.volumeData);
            slider.value = newData.volumeData;
        }
        else
        {
            string LoadData;
            GameData MyData;
            LoadData = File.ReadAllText(fileName);
            MyData = JsonUtility.FromJson<GameData>(LoadData);
            tempVolume = MyData.volumeData;
            isGameEnd = MyData.isGameEnd;
            resIdx = MyData.resIdx;
            isFullscreen = MyData.isFullscreen;
            setVolume(MyData.volumeData);
            slider.value = MyData.volumeData;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);
            if (isFullscreen)
            {
                fullButton.SetActive(true);
            }
            if (!isFullscreen)
            {
                fullButton.SetActive(false);
            }
        }

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused && !isOption)
            {
                Resume();
            }
            else if (GameIsPaused && isOption)
            {
                if (isContoller)
                {
                    Option();
                }
                else
                    MainMenu();
            }
            else
            {
                Pause();
            }
        }

        resText.text = res[resIdx].ToString();
    }
    public void Fullscreen()
    {
        isFullscreen = !isFullscreen;
        Screen.fullScreen = isFullscreen;
        if (!isFullscreen)
        {
            fullButton.SetActive(true);
            emptyButton.SetActive(false);
        }
        if (isFullscreen)
        {
            fullButton.SetActive(false);
            emptyButton.SetActive(true);
        }
    }
    public void NextRes()
    {
        if (resIdx >= 2)
        {
            resIdx = 0;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);

        }
        else
        {
            resIdx++;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);

        }

    }
    public void PrevRes()
    {

        if (resIdx <= 0)
        {
            resIdx = 2;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);

        }
        else
        {
            resIdx--;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);

        }
    }
    public void setVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
        tempVolume = volume;
    }
    public void Resume()
    {
        logo.SetActive(false);
        menuBackground.SetActive(false);
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1.0f;
        GameIsPaused = false;
    }
    public void Pause()
    {
        logo.SetActive(true);
        menuBackground.SetActive(true);
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0.0f;
        GameIsPaused = true;
    }

    public void QuitGame()
    {
        GameIsPaused = false;
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(0);
    }
    public void Option()
    {
        ControllerObject.SetActive(false);
        isContoller = false;
        isOption = true;
        OptionObject.SetActive(true);
        pauseMenuUI.SetActive(false);
    }
    public void Contoller()
    {
        ControllerObject.SetActive(true);
        OptionObject.SetActive(false);
        isContoller = true;
    }
    public void MainMenu()
    {
        isOption = false;
        GameData newData = new GameData
        {
            volumeData = tempVolume,
            isGameEnd = this.isGameEnd,
            isFullscreen = this.isFullscreen,
            resIdx = this.resIdx
        };
        if (!Directory.Exists(System.IO.Path.Combine(Application.dataPath, "Setting")))
        {
            Directory.CreateDirectory(System.IO.Path.Combine(Application.dataPath, "Setting"));
        }
        fileName = Path.Combine(System.IO.Path.Combine(Application.dataPath, "Setting"), fileName);
        if (!File.Exists(fileName))
        {
            newData.volumeData = 1.0f;
            FileStream fs = File.Create(fileName);
            fs.Close();
        }
        string saveString = JsonUtility.ToJson(newData);
        File.WriteAllText(fileName, saveString);
        OptionObject.SetActive(false);
        pauseMenuUI.SetActive(true);
    }
}
