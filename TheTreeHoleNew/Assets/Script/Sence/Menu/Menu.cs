﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public Slider slider;
    string fileName = "setting.json"; //賦值名

    public GameObject EndBG;
    public GameObject NotEndBG;
    public GameObject MainMenuObject;
    public GameObject OptionObject;
    public GameObject ControllerObject;
    public AudioMixer audioMixer;
    public GameObject emptyButton;
    public GameObject fullButton;
    public Text resText;
    private float tempVolume;
    private bool isFullscreen = true;
    public bool isOption = false;
    public bool isContoller = false;
    public bool isGameEnd = false;
    private string[] res = {
        "1920 x 1080",
        "1600 x 900",
        "1440 x 900"
    };

    class Reslo
    {
        public int width;
        public int height;
    }

    private Reslo[] resolutions ={
        new Reslo{ width =1920,height=1080},
        new Reslo{ width =1600,height=900},
        new Reslo{ width =1440,height=900}
    };
    private int resIdx = 0;

    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            Debug.Log(res[i] + " " + resolutions[i].width + " " + resolutions[i].height);
        }
        GameData newData = new GameData
        {
            isFullscreen = this.isFullscreen,
            volumeData = tempVolume,
            isGameEnd = this.isGameEnd,
            resIdx = this.resIdx
        };
        if (!Directory.Exists(System.IO.Path.Combine(Application.dataPath, "Setting")))
        {
            Directory.CreateDirectory(System.IO.Path.Combine(Application.dataPath, "Setting"));
        }
        fileName = Path.Combine(System.IO.Path.Combine(Application.dataPath, "Setting"), fileName);
        if (!File.Exists(fileName))
        {
            newData.volumeData = 1.0f;
            FileStream fs = File.Create(fileName);
            fs.Close();
            string saveString = JsonUtility.ToJson(newData);
            File.WriteAllText(fileName, saveString);
            setVolume(newData.volumeData);
            slider.value = newData.volumeData;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);
        }
        else
        {
            string LoadData;
            GameData MyData;
            LoadData = File.ReadAllText(fileName);
            MyData = JsonUtility.FromJson<GameData>(LoadData);
            tempVolume = MyData.volumeData;
            isGameEnd = MyData.isGameEnd;
            resIdx = MyData.resIdx;
            isFullscreen = MyData.isFullscreen;
            setVolume(MyData.volumeData);
            slider.value = MyData.volumeData;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);
            if (isFullscreen)
            {
                fullButton.SetActive(true);
            }
            if (!isFullscreen)
            {
                fullButton.SetActive(false);
            }
        }
        if (isGameEnd)
        {
            EndBG.SetActive(true);
            NotEndBG.SetActive(false);
        }
        else
        {
            EndBG.SetActive(false);
            NotEndBG.SetActive(true);
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isOption)
            {
                if (isContoller)
                {
                    Option();
                }
                else
                    MainMenu();
            }
        }
        resText.text = res[resIdx].ToString();
    }
    public void setVolume(float volume)
    {
        tempVolume = volume;
        audioMixer.SetFloat("volume", volume);
    }
    public void Fullscreen()
    {
        isFullscreen = !isFullscreen;
        Screen.fullScreen = isFullscreen;
        if (!isFullscreen)
        {
            fullButton.SetActive(true);
            emptyButton.SetActive(false);
        }
        if (isFullscreen)
        {
            fullButton.SetActive(false);
            emptyButton.SetActive(true);
        }
    }
    public void NextRes()
    {
        if (resIdx >= 2)
        {
            resIdx = 0;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);

        }
        else
        {
            resIdx++;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);

        }

    }
    public void PrevRes()
    {

        if (resIdx <= 0)
        {
            resIdx = 2;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);

        }
        else
        {
            resIdx--;
            Screen.SetResolution(resolutions[resIdx].width, resolutions[resIdx].height, isFullscreen);
        }
    }

    public void PlayyGame()
    {
        SceneManager.LoadScene(1);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void Option()
    {
        MainMenuObject.SetActive(false);
        ControllerObject.SetActive(false);
        OptionObject.SetActive(true);
        isOption = true;
        isContoller = false;
    }
    public void Contoller()
    {
        ControllerObject.SetActive(true);
        OptionObject.SetActive(false);
        isContoller = true;
    }
    public void MainMenu()
    {
        isOption = false;
        GameData newData = new GameData
        {
            volumeData = tempVolume,
            isGameEnd = this.isGameEnd,
            isFullscreen = this.isFullscreen,
            resIdx = this.resIdx
        };
        if (!Directory.Exists(System.IO.Path.Combine(Application.dataPath, "Setting")))
        {
            Directory.CreateDirectory(System.IO.Path.Combine(Application.dataPath, "Setting"));
        }
        fileName = Path.Combine(System.IO.Path.Combine(Application.dataPath, "Setting"), fileName);
        if (!File.Exists(fileName))
        {
            newData.volumeData = 1.0f;
            FileStream fs = File.Create(fileName);
            fs.Close();
        }
        string saveString = JsonUtility.ToJson(newData);
        File.WriteAllText(fileName, saveString);
        OptionObject.SetActive(false);
        MainMenuObject.SetActive(true);
        isOption = false;
    }

}
