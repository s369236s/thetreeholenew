﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firefly : MonoBehaviour
{
    private float speed = 0.1f;
    public float rotateSpeed = 5.0f;

    Vector3 newPosition;

    void Start()
    {

        PositionChange();
    }

    void PositionChange()
    {
        newPosition = new Vector2(transform.position.x + Random.Range(-1.0f, 1.0f), transform.position.y + Random.Range(-1.0f, 1.0f));
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, newPosition) < 1)
            PositionChange();

        transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * speed);

    }


}
