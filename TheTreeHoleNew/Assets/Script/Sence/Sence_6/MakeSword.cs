﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeSword : MonoBehaviour
{
    public DialogueTrigger dialogueTrigger;
    public Animator kentAnimator;
    void SetDialogueIsOpen()
    {
        GameManager.instance.isDialogueOpen = true;
    }
    void StartDialouge()
    {
        dialogueTrigger.TriggerDialogue();
    }
    void NameSword()
    {
        kentAnimator.Play("Name");
    }
}
