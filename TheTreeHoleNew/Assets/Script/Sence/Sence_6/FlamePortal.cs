﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamePortal : PickUpPortal
{
    new void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isHover && GameObject.FindGameObjectsWithTag("Bat").Length <= 0)
        {
            GameObject levelLoader = GameObject.Find("LevelLoader");
            LevelLoader lL = levelLoader.GetComponent<LevelLoader>();
            lL.LoadNextLevel(levelIndex + 1, 0);
            this.enabled = false;
        }
    }
}
