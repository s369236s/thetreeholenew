﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sence_6_End : MonoBehaviour
{
    public Animator animator;
    void EndThisSence()
    {
        if (GameManager.instance.dialogueStatus == 2)
        {
            GameManager.instance.isDialogueOpen = true;
            animator.SetTrigger("Start");
            GameObject levelLoader = GameObject.Find("LevelLoader");
            LevelLoader lL = levelLoader.GetComponent<LevelLoader>();
            lL.LoadNextLevel(15, 0);
            this.enabled = false;
        }
    }
}
