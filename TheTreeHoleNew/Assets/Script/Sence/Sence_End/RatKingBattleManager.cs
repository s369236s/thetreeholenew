﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatKingBattleManager : MonoBehaviour
{
    static public RatKingBattleManager instance;
    public bool isBattleBegun = false;
    public DialogueTrigger startDialogueTrigger;
    void Awake()
    {
        instance = this;
    }
    IEnumerator Start()
    {
        GameManager.instance.isDialogueOpen = true;
        yield return null;
        startDialogueTrigger.TriggerDialogue();
    }


}
