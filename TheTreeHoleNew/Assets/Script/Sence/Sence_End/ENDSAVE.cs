﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ENDSAVE : MonoBehaviour
{
    string fileName = "setting.json";
    void Start()
    {
        fileName = Path.Combine(System.IO.Path.Combine(Application.dataPath, "Setting"), fileName);
        string LoadData;
        GameData MyData;
        LoadData = File.ReadAllText(fileName);
        MyData = JsonUtility.FromJson<GameData>(LoadData);
        float tempVolume = MyData.volumeData;
        GameData newData = new GameData
        {
            volumeData = tempVolume,
            isGameEnd = true
        };
        string saveString = JsonUtility.ToJson(newData);
        File.WriteAllText(fileName, saveString);
    }


}
