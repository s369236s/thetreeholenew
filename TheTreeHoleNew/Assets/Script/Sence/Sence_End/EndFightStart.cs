﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndFightStart : MonoBehaviour
{
    public Animator animator;
    public int levelIndex;
    void FightBegin()
    {
        RatKingBattleManager.instance.isBattleBegun = true;
    }
    void TheEnd()
    {
        RatKingBattleManager.instance.isBattleBegun = false;
        GameManager.instance.isDialogueOpen = true;
        animator.SetTrigger("Start");
        GameObject levelLoader = GameObject.Find("LevelLoader");
        LevelLoader lL = levelLoader.GetComponent<LevelLoader>();
        lL.LoadNextLevel(levelIndex, 2);
        this.enabled = false;
    }
}
