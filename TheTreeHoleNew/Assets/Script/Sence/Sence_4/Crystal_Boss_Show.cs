﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class Crystal_Boss_Show : MonoBehaviour
{
    public DialogueTrigger firstDialogue;
    public Light2D light2D;
    void Start()
    {

    }

    void Update()
    {

    }
    public void BossAppearTriggerDialogue()
    {
        light2D.intensity = 1.13f;
        firstDialogue.TriggerDialogue();
    }
}
