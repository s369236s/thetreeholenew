﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class EndEveryLight : MonoBehaviour
{
    public Light2D kentLight;
    public Light2D bossLight;

    protected void EndLight()
    {
        kentLight.enabled = false;
        bossLight.enabled = false;
    }

}
