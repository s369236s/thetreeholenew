﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class TriggerBossShow : MonoBehaviour
{

    public Light2D bossLight2D;
    public float intensityAmount;

    public DialogueTrigger dialogueTrigger;

    private bool isShining = false;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Kent"))
        {
            GameManager.instance.isDialogueOpen = true;
            isShining = true;
        }
    }
    void Update()
    {
        if (isShining)
        {
            bossLight2D.intensity += intensityAmount * Time.deltaTime;
            if (bossLight2D.intensity >= 1.5)
            {
                isShining = false;
                dialogueTrigger.TriggerDialogue();
                GetComponent<BoxCollider2D>().enabled = false;
                this.enabled = false;
                Destroy(gameObject);
            }
        }
    }
}
