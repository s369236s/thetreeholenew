﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class CrystalBattle : MonoBehaviour
{
    public Light2D kentLight2D;
    public Light2D bossLight2D;

    void TurnOffLight()
    {
        kentLight2D.enabled = false;
        bossLight2D.enabled = false;
        CrystalBossManager.instance.isBattleBegun = true;
    }
}
