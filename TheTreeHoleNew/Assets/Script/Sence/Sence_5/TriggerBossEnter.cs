﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBossEnter : MonoBehaviour
{
    public Animator animator;
    public float moveSpeed;
    public Transform bossPosition;
    public Transform stopPosition;

    public bool isTrigger = false;
    public DialogueTrigger dialogueTrigger;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Kent"))
        {
            isTrigger = true;
            animator.SetFloat("Speed", 1);
            bossPosition.Rotate(0, 180f, 0);
            GameManager.instance.isDialogueOpen = true;

        }
    }

    void Update()
    {
        if (isTrigger)
        {
            bossPosition.position = Vector2.MoveTowards(bossPosition.position, new Vector2(stopPosition.position.x, bossPosition.position.y), moveSpeed * Time.deltaTime);
            if (bossPosition.position == stopPosition.position)
            {
                animator.SetFloat("Speed", 0);
                dialogueTrigger.TriggerDialogue();
                Destroy(this.gameObject);
            }
        }
    }
}
