﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartFight : MonoBehaviour
{
    public Transform bossPosition;
    void FightBegin()
    {
        bossPosition.Rotate(0, 180f, 0);
        SwordRatBossManager.instance.isBattleBegun = true;
    }
}
