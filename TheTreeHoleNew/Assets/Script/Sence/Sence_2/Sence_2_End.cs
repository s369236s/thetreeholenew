﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Sence_2_End : MonoBehaviour
{
    public GameObject fadeBlack;
    void Update()
    {
        if (GameManager.instance.dialogueStatus == 3)
        {
            fadeBlack.SetActive(true);
            GameObject levelLoader = GameObject.Find("LevelLoader");
            LevelLoader lL = levelLoader.GetComponent<LevelLoader>();
            lL.LoadNextLevel(3, 0);
            this.enabled = false;
        }
    }
}
