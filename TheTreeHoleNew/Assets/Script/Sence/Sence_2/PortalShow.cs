﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalShow : MonoBehaviour
{
    public GameObject portal;
    public int dialogueStatus;
    void Update()
    {
        if (GameManager.instance.dialogueStatus == dialogueStatus)
        {
            portal.SetActive(true);
            this.enabled = false;
        }
    }
}
