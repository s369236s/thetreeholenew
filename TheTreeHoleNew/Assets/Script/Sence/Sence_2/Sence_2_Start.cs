﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sence_2_Start : MonoBehaviour
{
    public Animator kent_Bed_Animator;
    public Animator cameraMan_Animator;
    public GameObject exclamationMark;

    public DialogueTrigger dialogueTrigger;
    // public DialogueTrigger dialogueTrigger_2;
    IEnumerator Start()
    {
        yield return new WaitForSeconds(3f);
        kent_Bed_Animator.SetTrigger("Wake");
        exclamationMark.SetActive(true);
        yield return new WaitForSeconds(2f);
        Destroy(exclamationMark);
        dialogueTrigger.TriggerDialogue();
        // yield return new WaitForSeconds(0.3f);
        // cameraMan_Animator.SetTrigger("Move_1");
        // yield return new WaitForSeconds(2f);

    }


}
