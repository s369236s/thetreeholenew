﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SenceStart : MonoBehaviour
{

    public DialogueTrigger startTrigger;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.1f);
        startTrigger.TriggerDialogue();
    }
}
