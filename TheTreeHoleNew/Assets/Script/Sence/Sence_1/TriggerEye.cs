﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEye : MonoBehaviour
{


    public GameObject gameObject_1;
    public GameObject gameObject_2;
    public GameObject gameObject_3;
    public GameObject gameObject_4;
    public GameObject gameObject_5;
    public GameObject gameObject_6;
    public GameObject gameObject_7;
    public GameObject gameObject_8;
    public GameObject gameObject_Red;

    private bool isTrigger = false;

    public Canvas fadeCanvas;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Kent" && !isTrigger)
        {
            isTrigger = true;
            StartCoroutine(Wait(gameObject_1));
            StartCoroutine(Wait(gameObject_2));
            StartCoroutine(Wait(gameObject_3));
            StartCoroutine(Wait(gameObject_4));
            StartCoroutine(Wait(gameObject_5));
            StartCoroutine(Wait(gameObject_6));
            StartCoroutine(Wait(gameObject_7));
            StartCoroutine(Wait(gameObject_8));
            StartCoroutine(Wait(gameObject_Red));
            fadeCanvas.gameObject.SetActive(true);
            GameObject levelLoader = GameObject.Find("LevelLoader");
            LevelLoader lL = levelLoader.GetComponent<LevelLoader>();
            lL.LoadNextLevel(2, 3);
            GameManager.instance.isDialogueOpen = true;
            Destroy(gameObject);
        }
    }
    IEnumerator Wait(GameObject gameObject)
    {
        gameObject.SetActive(true);
        yield return new WaitForSeconds(0.15f);
    }
}
