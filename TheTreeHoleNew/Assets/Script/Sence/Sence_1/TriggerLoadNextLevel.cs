﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerLoadNextLevel : MonoBehaviour
{

    public int nextLevelIndex = 0;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Kent")
        {
            GameObject levelLoader = GameObject.Find("LevelLoader");
            LevelLoader lL = levelLoader.GetComponent<LevelLoader>();
            GameManager.instance.isDialogueOpen = true;
            lL.LoadNextLevel(1, 0);
            Destroy(gameObject);
        }
    }
}
