﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameData
{
    public float volumeData;
    public float HP;
    public bool isGameEnd = false;
    public int resIdx;
    public bool isFullscreen;
}

public class DataLoader : MonoBehaviour
{
    public static DataLoader instance;
    public float volumeData;
    void Awake()
    {
        instance = this;
    }
    void SaveData()
    {
        GameData newData = new GameData
        {
            volumeData = this.volumeData,
        };

        string jsonInfo = JsonUtility.ToJson(newData, true);
        File.WriteAllText("C:/TheTreeHoleSave/Setting", jsonInfo);

    }
    void SaveVolume(float volume)
    {
        volumeData = volume;
        SaveData();
    }

}

